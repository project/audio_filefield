
**********************************

Audio FileField

**********************************
Initial development:
May 3, 2008
by Aaron Winborn
Advomatic

http://aaronwinborn.com/ (my blog)
http://advomatic.com/ (my day job)

**********************************

This module adds Audio support to the FileField module for CCK.

In particular, it adds support for audio metadata, and allows automatic audio playback, if using the jQuery Media module plugin.

TODO:
collect and store metadata
audio playback theming
plugin to jQuery Media
